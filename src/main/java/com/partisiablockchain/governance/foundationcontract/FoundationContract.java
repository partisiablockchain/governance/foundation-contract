package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.stream.Collectors;

/** Contract for controlling pool of tokens. */
@AutoSysContract(FoundationContractState.class)
public final class FoundationContract {

  static final class Invocations {

    static final int SIGN_TRANSFER = 0;
    static final int BURN_TOKENS = 1;
    static final int APPROVE = 2;
    static final int MINT = 3;
    static final int REPLACE_COUNCIL_KEYS = 4;

    private Invocations() {}
  }

  /**
   * Initializes the contract.
   *
   * @param pooledTokens the amount of tokens to initialize the contract with
   * @param foundationMembers the foundation members of this contract, who signs the transfers
   * @return a contract state object initialized with the supplied parameters
   */
  @Init
  public FoundationContractState create(
      long pooledTokens, List<BlockchainPublicKey> foundationMembers) {
    ensure(foundationMembers.size() == 5, "Foundation is expected to consist of five members");
    return FoundationContractState.initial(pooledTokens, FixedList.create(foundationMembers));
  }

  /**
   * Migrates an existing contract to this version.
   *
   * @param oldState state accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public FoundationContractState upgrade(StateAccessor oldState) {
    return FoundationContractState.upgrade(oldState);
  }

  /**
   * Replaces all the keys in the council.
   *
   * @param context execution context
   * @param state current state of the contract
   * @param foundationMembers foundation member keys and signing of these to prove ownership
   * @param signatures enough signatures from the old council to shift into the new council
   * @return state with new council and a bumped nonce
   */
  @Action(Invocations.REPLACE_COUNCIL_KEYS)
  public FoundationContractState replaceCouncilKeys(
      SysContractContext context,
      FoundationContractState state,
      List<NewCouncilKey> foundationMembers,
      List<Signature> signatures) {

    ensure(foundationMembers.size() >= 4, "Need at least 4 members in the foundation council");
    Hash message = state.createReplaceMessage(foundationMembers, context.getContractAddress());
    ensure(
        new FoundationMemberSignatureVerifier(state.getFoundationMembers())
            .verify(message, signatures),
        "At least three foundation members should have signed the replacement");
    for (NewCouncilKey foundationMember : foundationMembers) {
      ensure(
          foundationMember.checkValidPossessionProof(),
          "All keys must be valid, i.e. able to do signing");
    }
    int size =
        foundationMembers.stream().map(NewCouncilKey::publicKey).collect(Collectors.toSet()).size();
    ensure(size == foundationMembers.size(), "All keys must be unique");

    return state
        .replaceCouncilKeys(foundationMembers.stream().map(NewCouncilKey::publicKey).toList())
        .bumpNonce();
  }

  /**
   * Execute a list of transfers if there are enough tokens in the pool and the corresponding
   * message hash (see {@link FoundationContractState#createTransferMessage}) have been signed by at
   * least three foundation members.
   *
   * @param context execution context
   * @param state current state of the contract
   * @param transfers transfers to execute
   * @param signatures foundation member signatures
   * @return state with pool reduced by total token amount of transfers and a bumped nonce
   */
  @Action(Invocations.SIGN_TRANSFER)
  public FoundationContractState signTransfer(
      SysContractContext context,
      FoundationContractState state,
      List<Transfer> transfers,
      List<Signature> signatures) {
    ensure(
        transfers.stream().noneMatch(t -> t.amount() <= 0),
        "No transfer amount can be negative or zero");

    long totalTransferAmount = calculateTotalTransferAmount(transfers);
    ensure(
        state.hasEnoughTokens(totalTransferAmount),
        "Insufficient remaining tokens in pool for transfers");

    Hash message = state.createTransferMessage(transfers, context.getContractAddress());
    ensure(
        new FoundationMemberSignatureVerifier(state.getFoundationMembers())
            .verify(message, signatures),
        "At least three foundation members should have signed the transfers");

    executeTransfers(transfers, context);
    return state.subtractFromPool(totalTransferAmount).bumpNonce();
  }

  private static void executeTransfers(List<Transfer> transfers, SysContractContext context) {
    for (Transfer transfer : transfers) {
      context
          .getInvocationCreator()
          .updateLocalAccountPluginState(
              LocalPluginStateUpdate.create(
                  transfer.recipient(),
                  AccountPluginRpc.createVestingAccount(
                      transfer.amount(), transfer.vestingSchedule())));
    }
  }

  private long calculateTotalTransferAmount(List<Transfer> transfers) {
    return transfers.stream().mapToLong(Transfer::amount).sum();
  }

  /**
   * Burn a part of this pool if there are enough tokens in the pool and the corresponding message
   * hash (see {@link FoundationContractState#createBurnMessage}) have been signed by at least three
   * foundation members.
   *
   * @param context execution context
   * @param state current state of the contract
   * @param amount amount of tokens to burn
   * @param signatures foundation member signatures
   * @return state with pool reduced by burn amount and a bumped nonce
   */
  @Action(Invocations.BURN_TOKENS)
  public FoundationContractState burnTokens(
      SysContractContext context,
      FoundationContractState state,
      long amount,
      List<Signature> signatures) {
    ensure(state.hasEnoughTokens(amount), "Insufficient remaining tokens in pool to burn");
    Hash message = state.createBurnMessage(amount, context.getContractAddress());
    ensure(
        new FoundationMemberSignatureVerifier(state.getFoundationMembers())
            .verify(message, signatures),
        "At least three foundation members should have signed the burn");
    return state.burnTokens(amount);
  }

  /**
   * Approve an address to mint up to a specified amount of tokens from the token pool if the
   * corresponding message hash (see {@link FoundationContractState#createApproveMessage}) have been
   * signed by at least three foundation members.
   *
   * @param context execution context
   * @param state current state of the contract
   * @param approvals the approvals to execute
   * @param signatures foundation member signatures
   * @return state with added allowances and a bumped nonce
   */
  @Action(Invocations.APPROVE)
  public FoundationContractState approve(
      SysContractContext context,
      FoundationContractState state,
      List<Approval> approvals,
      List<Signature> signatures) {
    Hash message = state.createApproveMessage(approvals, context.getContractAddress());
    ensure(
        new FoundationMemberSignatureVerifier(state.getFoundationMembers())
            .verify(message, signatures),
        "At least three foundation members should have signed the approval");
    return state.approve(approvals).bumpNonce();
  }

  /**
   * Execute a list of mints if the total minted amount does not exceed the pool size or the
   * allowance of the sender.
   *
   * @param context execution context
   * @param state current state of the contract
   * @param mints the mints to execute
   * @return state with pool reduced by total token amount of mints, allowance of sender reduced by
   *     total token amount of mints, and a bumped nonce.
   */
  @Action(Invocations.MINT)
  public FoundationContractState mint(
      SysContractContext context, FoundationContractState state, List<Mint> mints) {
    ensure(
        mints.stream().noneMatch(m -> m.amount() <= 0), "No mint amount can be negative or zero");

    long totalMintAmount = calculateTotalMintAmount(mints);
    ensure(
        state.hasEnoughTokens(totalMintAmount), "Insufficient remaining tokens in pool for mints");

    BlockchainAddress spender = context.getFrom();
    ensure(state.hasEnoughAllowance(spender, totalMintAmount), "Insufficient allowance for mints");

    executeMints(mints, context);
    return state.subtractFromPool(totalMintAmount).reduceAllowance(spender, totalMintAmount);
  }

  private static void executeMints(List<Mint> mints, SysContractContext context) {
    for (Mint mint : mints) {
      context
          .getInvocationCreator()
          .updateLocalAccountPluginState(
              LocalPluginStateUpdate.create(
                  mint.recipient(), AccountPluginRpc.addMpcTokenBalance(mint.amount())));
    }
  }

  private long calculateTotalMintAmount(List<Mint> mints) {
    return mints.stream().mapToLong(Mint::amount).sum();
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }
}
