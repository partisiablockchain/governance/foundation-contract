package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Verify signatures of a given message hash. The signatures have to be from known foundation
 * members.
 */
final class FoundationMemberSignatureVerifier {

  private final Collection<BlockchainPublicKey> foundationMembers;

  /**
   * Default constructor.
   *
   * @param foundationMembers all foundation members
   */
  FoundationMemberSignatureVerifier(List<BlockchainPublicKey> foundationMembers) {
    this.foundationMembers = foundationMembers;
  }

  /**
   * Verify that at least three unique signatures are of the expected message and from foundation
   * members.
   *
   * @param message expected signature message
   * @param signatures signatures to verify
   * @return true if three or more unique signatures are of the given message and from foundation
   *     members.
   */
  public boolean verify(Hash message, List<Signature> signatures) {
    HashSet<BlockchainPublicKey> signers = new HashSet<>();
    for (Signature signature : signatures) {
      BlockchainPublicKey recovered = signature.recoverPublicKey(message);
      if (foundationMembers.contains(recovered)) {
        signers.add(recovered);
      }
    }
    return signers.size() >= 3;
  }
}
