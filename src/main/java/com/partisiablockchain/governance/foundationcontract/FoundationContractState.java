package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Objects;

/** State for the {@link FoundationContract}. */
@Immutable
public final class FoundationContractState implements StateSerializable {

  /** Domain separation used in signature messages. */
  public static final String DOMAIN_SEPARATOR = "PBC Foundation";

  /**
   * The nonce is part of the message signed by foundation members and is bumped after every
   * executed foundation operation.
   */
  public final long nonce;

  /** Amount of remaining tokens in the pool. */
  public final long remainingTokens;

  /** The foundation members that are able to sign foundation operations. */
  public final FixedList<BlockchainPublicKey> foundationMembers;

  /** Allows specific addresses to transfer up to a specified amount from the pool. */
  public final AvlTree<BlockchainAddress, Long> allowances;

  @SuppressWarnings("unused")
  private FoundationContractState() {
    nonce = 0;
    remainingTokens = 0;
    foundationMembers = null;
    allowances = null;
  }

  FoundationContractState(
      long nonce,
      long remainingTokens,
      FixedList<BlockchainPublicKey> foundationMembers,
      AvlTree<BlockchainAddress, Long> allowances) {
    this.nonce = nonce;
    this.remainingTokens = remainingTokens;
    this.foundationMembers = foundationMembers;
    this.allowances = allowances;
  }

  static FoundationContractState initial(
      long pooledTokens, FixedList<BlockchainPublicKey> foundationMembers) {
    return new FoundationContractState(0, pooledTokens, foundationMembers, AvlTree.create());
  }

  static FoundationContractState upgrade(StateAccessor oldState) {
    long nonce = oldState.get("nonce").longValue();
    long remainingTokens = oldState.get("remainingTokens").longValue();
    FixedList<BlockchainPublicKey> foundationMembers =
        oldState.get("foundationMembers").typedFixedList(BlockchainPublicKey.class);
    AvlTree<BlockchainAddress, Long> allowances = AvlTree.create();
    if (oldState.hasField("allowances")) {
      allowances = oldState.get("allowances").typedAvlTree(BlockchainAddress.class, Long.class);
    }
    return new FoundationContractState(nonce, remainingTokens, foundationMembers, allowances);
  }

  /**
   * Replace the keys for the council.
   *
   * @param foundationCouncilMembers the members of the council
   * @return update state with new council
   */
  public FoundationContractState replaceCouncilKeys(
      List<BlockchainPublicKey> foundationCouncilMembers) {
    return new FoundationContractState(
        nonce, remainingTokens, FixedList.create(foundationCouncilMembers), allowances);
  }

  boolean hasEnoughTokens(long amount) {
    return remainingTokens >= amount;
  }

  FoundationContractState subtractFromPool(long amount) {
    return stateBuilder().subtractFromRemainingTokens(amount).build();
  }

  FoundationContractState reduceAllowance(BlockchainAddress spender, long totalMintAmount) {
    return stateBuilder().reduceAllowance(spender, totalMintAmount).build();
  }

  FoundationContractState bumpNonce() {
    return stateBuilder().bumpNonce().build();
  }

  FoundationContractState burnTokens(long amount) {
    return stateBuilder().bumpNonce().subtractFromRemainingTokens(amount).build();
  }

  FoundationContractState approve(List<Approval> approvals) {
    return stateBuilder().addAllowances(approvals).build();
  }

  private FoundationContractStateBuilder stateBuilder() {
    return new FoundationContractStateBuilder(this);
  }

  long getAllowance(BlockchainAddress spender) {
    if (allowances.containsKey(spender)) {
      return allowances.getValue(spender);
    } else {
      return 0;
    }
  }

  boolean hasEnoughAllowance(BlockchainAddress spender, long totalMintAmount) {
    return getAllowance(spender) >= totalMintAmount;
  }

  /** Builder for mutating state. */
  private static final class FoundationContractStateBuilder {

    private long nonce;
    private long remainingTokens;
    private final FixedList<BlockchainPublicKey> foundationMembers;
    private AvlTree<BlockchainAddress, Long> allowances;

    private FoundationContractStateBuilder(FoundationContractState state) {
      nonce = state.nonce;
      remainingTokens = state.remainingTokens;
      foundationMembers = state.foundationMembers;
      allowances = state.allowances;
    }

    private FoundationContractStateBuilder bumpNonce() {
      nonce += 1;
      return this;
    }

    private FoundationContractStateBuilder subtractFromRemainingTokens(long amount) {
      remainingTokens -= amount;
      return this;
    }

    private FoundationContractStateBuilder addAllowances(List<Approval> approvals) {
      for (Approval approval : approvals) {
        BlockchainAddress spender = approval.spender();
        long existingAllowance = 0;
        if (allowances.containsKey(spender)) {
          existingAllowance = allowances.getValue(spender);
        }
        long updatedAllowance = existingAllowance + approval.amount();
        if (updatedAllowance > 0) {
          allowances = allowances.set(spender, updatedAllowance);
        } else {
          allowances = allowances.remove(spender);
        }
      }
      return this;
    }

    private FoundationContractState build() {
      return new FoundationContractState(nonce, remainingTokens, foundationMembers, allowances);
    }

    private FoundationContractStateBuilder reduceAllowance(
        BlockchainAddress spender, long totalMintAmount) {
      long updatedAllowance = allowances.getValue(spender) - totalMintAmount;
      if (updatedAllowance > 0) {
        allowances = allowances.set(spender, updatedAllowance);
      } else {
        allowances = allowances.remove(spender);
      }
      return this;
    }
  }

  /**
   * Creates the message hash for foundation members to sign for a 'transfer' operation.
   *
   * <p>The message contains:
   *
   * <ul>
   *   <li>the transfers to execute
   *   <li>the domain separator (see {@link #DOMAIN_SEPARATOR})
   *   <li>the address of the foundation contract
   *   <li>the current nonce (see {@link #nonce})
   * </ul>
   *
   * @param transfers transfers to execute
   * @param contractAddress address of the foundation contract
   * @return message hash for foundation members to sign for a 'transfer' operation.
   */
  Hash createTransferMessage(List<Transfer> transfers, BlockchainAddress contractAddress) {
    return Hash.create(
        h -> {
          transfers.forEach(t -> t.write(h));
          h.writeString(DOMAIN_SEPARATOR);
          contractAddress.write(h);
          h.writeLong(nonce);
        });
  }

  /**
   * Creates the message hash for foundation members council to sign for a 'replaceCouncilKeys'
   * operation.
   *
   * <p>The message contains:
   *
   * <ul>
   *   <li>the string "REPLACE"
   *   <li>the key rotation to execute
   *   <li>the domain separator (see {@link #DOMAIN_SEPARATOR})
   *   <li>the address of the foundation contract
   *   <li>the current nonce (see {@link #nonce})
   * </ul>
   *
   * @param newFoundationCouncil new foundation council members
   * @param contractAddress address of the foundation contract
   * @return message hash for foundation members to sign for a 'replace' operation.
   */
  Hash createReplaceMessage(
      List<NewCouncilKey> newFoundationCouncil, BlockchainAddress contractAddress) {
    return Hash.create(
        h -> {
          h.writeString("REPLACE");
          newFoundationCouncil.forEach(t -> t.write(h));
          h.writeString(DOMAIN_SEPARATOR);
          contractAddress.write(h);
          h.writeLong(nonce);
        });
  }

  /**
   * Creates the message hash for foundation members to sign for a 'burn' operation.
   *
   * <p>The message contains:
   *
   * <ul>
   *   <li>the string "BURN"
   *   <li>the amount of tokens to burn
   *   <li>the domain separator (see {@link #DOMAIN_SEPARATOR})
   *   <li>the address of the foundation contract
   *   <li>the current nonce (see {@link #nonce})
   * </ul>
   *
   * @param amount amount of tokens to burn
   * @param contractAddress address of the foundation contract
   * @return message hash for foundation members to sign for a 'burn' operation.
   */
  Hash createBurnMessage(long amount, BlockchainAddress contractAddress) {
    return Hash.create(
        h -> {
          h.writeString("BURN");
          h.writeLong(amount);
          h.writeString(DOMAIN_SEPARATOR);
          contractAddress.write(h);
          h.writeLong(nonce);
        });
  }

  /**
   * Creates the message hash for foundation members to sign for an 'approve' operation.
   *
   * <p>The message contains:
   *
   * <ul>
   *   <li>the string "APPROVE"
   *   <li>the approvals to execute
   *   <li>the domain separator (see {@link #DOMAIN_SEPARATOR})
   *   <li>the address of the foundation contract
   *   <li>the current nonce (see {@link #nonce})
   * </ul>
   *
   * @param approvals approvals to execute
   * @param contractAddress address of the foundation contract
   * @return message hash for foundation members to sign for an 'approve' operation.
   */
  Hash createApproveMessage(List<Approval> approvals, BlockchainAddress contractAddress) {
    return Hash.create(
        h -> {
          h.writeString("APPROVE");
          approvals.forEach(a -> a.write(h));
          h.writeString(DOMAIN_SEPARATOR);
          contractAddress.write(h);
          h.writeLong(nonce);
        });
  }

  List<BlockchainPublicKey> getFoundationMembers() {
    return Objects.requireNonNull(foundationMembers).stream().toList();
  }
}
