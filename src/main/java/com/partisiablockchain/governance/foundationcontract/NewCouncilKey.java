package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;

/**
 * Contains a new key used when the council is replaced. Key replacement can be triggered by the old
 * council, then all foundation council keys are updated and replaced.
 *
 * @param publicKey the new public key
 * @param possessionProof the signature proving possession of the private key, the signer must sign
 *     using the private key the public key
 */
@Immutable
public record NewCouncilKey(BlockchainPublicKey publicKey, Signature possessionProof)
    implements DataStreamSerializable {

  @Override
  public void write(SafeDataOutputStream outputStream) {
    publicKey().write(outputStream);
    possessionProof().write(outputStream);
  }

  /**
   * Checks that this new key is valid (i.e. that the signature is valid and proves ownership of the
   * private key).
   *
   * @return true if valid
   */
  public boolean checkValidPossessionProof() {
    BlockchainPublicKey signingPublicKey =
        possessionProof().recoverPublicKey(createUniqueProofHash(publicKey()));
    return signingPublicKey.equals(publicKey());
  }

  static Hash createUniqueProofHash(BlockchainPublicKey publicKey) {
    return Hash.create(publicKey::write);
  }
}
