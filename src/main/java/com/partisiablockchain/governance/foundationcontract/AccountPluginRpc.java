package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataOutputStream;

final class AccountPluginRpc {

  static final byte ADD_MPC_TOKEN_BALANCE = 2;
  static final byte CREATE_VESTING_ACCOUNT = 15;

  @SuppressWarnings("unused")
  private AccountPluginRpc() {}

  static byte[] addMpcTokenBalance(long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ADD_MPC_TOKEN_BALANCE);
          stream.writeLong(amount);
        });
  }

  static byte[] createVestingAccount(long amount, VestingSchedule vestingSchedule) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(CREATE_VESTING_ACCOUNT);
          stream.writeLong(amount);
          vestingSchedule.write(stream);
        });
  }
}
