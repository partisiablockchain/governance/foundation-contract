package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/** Test for {@link FoundationContract}. */
public final class FoundationContractTest {

  private static final BlockchainAddress ACCOUNT_A =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("A")));
  private static final BlockchainAddress ACCOUNT_B =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("B")));
  private static final BlockchainAddress ACCOUNT_C =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("C")));

  private static final VestingSchedule VESTING_SCHEDULE_DUMMY = new VestingSchedule(1, 2, 3);
  private static final List<Transfer> TRANSFERS_DUMMY =
      List.of(
          new Transfer(ACCOUNT_A, 1111, VESTING_SCHEDULE_DUMMY),
          new Transfer(ACCOUNT_B, 2222, VESTING_SCHEDULE_DUMMY));

  private final SysContractSerialization<FoundationContractState> serialization =
      new SysContractSerialization<>(
          FoundationContractInvoker.class, FoundationContractState.class);

  private SysContractContextTest context = new SysContractContextTest(1, 100, ACCOUNT_A);

  // Feature: Create

  /**
   * Contract is initialized with five foundation members and a token pool. The nonce is zero and
   * there are no allowances.
   */
  @Test
  public void initialize() {
    long pooledTokens = 1000;
    List<BlockchainPublicKey> foundationMembers = createFoundationMembers();

    FoundationContractState expected =
        FoundationContractState.initial(pooledTokens, FixedList.create(foundationMembers));

    FoundationContractState initialized =
        serialization.create(
            context,
            s -> {
              s.writeLong(pooledTokens);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(s, foundationMembers);
            });

    StateSerializableEquality.assertStatesEqual(initialized, expected);
  }

  /** Contract can only be initialized with exactly five foundation members. */
  @ParameterizedTest
  @ValueSource(ints = {0, 4, 6})
  public void initializeWrongNumberOfFoundationMembers(int noMembers) {
    List<BlockchainPublicKey> foundationMembers = createFoundationMembers(noMembers);
    Assertions.assertThatThrownBy(
            () ->
                serialization.create(
                    context,
                    s -> {
                      s.writeLong(1000);
                      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(s, foundationMembers);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Foundation is expected to consist of five members");
  }

  /** Contract state is migrated on upgrade. */
  @Test
  public void onUpgrade() {
    long remainingTokens = 1000;
    FixedList<BlockchainPublicKey> foundationMembers = FixedList.create(createFoundationMembers());
    AvlTree<BlockchainAddress, Long> allowances =
        AvlTree.create(Map.of(ACCOUNT_A, 1111L, ACCOUNT_B, 2222L));
    FoundationContractState expected =
        new FoundationContractState(1234, remainingTokens, foundationMembers, allowances);

    StateAccessor accessor = StateAccessor.create(expected);
    FoundationContractState updated = new FoundationContract().upgrade(accessor);

    StateSerializableEquality.assertStatesEqual(updated, expected);
  }

  /**
   * Contract state without allowances field is migrated on upgrade. The allowances field is
   * initialized to an empty AVL tree and the rest of the fields stay the same.
   */
  @Test
  public void onUpgradeWithoutAllowances() {
    long remainingTokens = 1000;
    FixedList<BlockchainPublicKey> foundationMembers = FixedList.create(createFoundationMembers());

    FoundationContractStateWithoutAllowances expected =
        new FoundationContractStateWithoutAllowances(1234, remainingTokens, foundationMembers);

    StateAccessor accessor = StateAccessor.create(expected);
    FoundationContractState updated = new FoundationContract().upgrade(accessor);

    Assertions.assertThat(getAllowances(updated).size()).isEqualTo(0);

    RecursiveComparisonConfiguration config =
        StateSerializableEquality.ASSERTIONS_CONFIG_STATE_SERIALIZABLE;
    config.strictTypeChecking(false);
    Assertions.assertThat(updated)
        .usingRecursiveComparison(config)
        .ignoringFields("allowances")
        .isEqualTo(expected);
  }

  private record FoundationContractStateWithoutAllowances(
      long nonce, long remainingTokens, FixedList<BlockchainPublicKey> foundationMembers)
      implements StateSerializable {}

  /**
   * A transfer with three signatures from foundation members is executed. The corresponding account
   * plugin events are spawned, the total transferred amount is subtracted from the pool, and the
   * nonce is bumped.
   *
   * <p>The total amount of tokens to transfer can be less than or equal to the amount of remaining
   * tokens in the pool.
   */
  @ParameterizedTest
  @ValueSource(ints = {49, 50})
  void transferSignedByAtLeastThreeFoundationMembers(int amountA) {
    int remainingTokens = 100;
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(remainingTokens, foundationMemberKeys);

    long amountB = 50;
    Transfer transferA = new Transfer(ACCOUNT_A, amountA, VESTING_SCHEDULE_DUMMY);
    Transfer transferB = new Transfer(ACCOUNT_B, amountB, VESTING_SCHEDULE_DUMMY);
    List<Transfer> transfers = List.of(transferA, transferB);
    Hash message = initial.createTransferMessage(transfers, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMemberKeys.subList(0, 3), message);

    FoundationContractState afterTransfer = invokeSignTransfer(initial, transfers, signatures);

    for (int i = 0; i < transfers.size(); i++) {
      Transfer transfer = transfers.get(i);
      LocalPluginStateUpdate localPluginStateUpdate =
          context.getUpdateLocalAccountPluginStates().get(i);
      Assertions.assertThat(localPluginStateUpdate.getContext()).isEqualTo(transfer.recipient());
      byte[] actualRpc = localPluginStateUpdate.getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.createVestingAccount(transfer.amount(), transfer.vestingSchedule());
      Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);
    }

    Assertions.assertThat(getRemainingTokens(afterTransfer))
        .isEqualTo(remainingTokens - totalTransferAmount(transfers));

    Assertions.assertThat(getNonce(afterTransfer)).isEqualTo(getNonce(initial) + 1);
  }

  /**
   * A transfer fails if the total amount of tokens to transfer exceeds the amount of remaining
   * tokens in the pool.
   */
  @Test
  void transferInsufficientTokensInPool() {
    int remainingTokens = 1;
    List<KeyPair> foundationMembers = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(remainingTokens, foundationMembers);

    Transfer transferA = new Transfer(ACCOUNT_A, 1, VESTING_SCHEDULE_DUMMY);
    Transfer transferB = new Transfer(ACCOUNT_B, 1, VESTING_SCHEDULE_DUMMY);
    List<Transfer> transfers = List.of(transferA, transferB);
    Hash message = initial.createTransferMessage(transfers, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMembers.subList(0, 3), message);

    Assertions.assertThat(totalTransferAmount(transfers))
        .isGreaterThan(getRemainingTokens(initial));
    Assertions.assertThatThrownBy(() -> invokeSignTransfer(initial, transfers, signatures))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Insufficient remaining tokens in pool for transfers");
  }

  /** A transfer of zero or a negative amount fails. */
  @ParameterizedTest
  @ValueSource(ints = {-1, 0})
  void transferNegativeOrZero(int negativeOrZeroAmount) {
    List<KeyPair> foundationMembers = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(Long.MAX_VALUE, foundationMembers);

    Transfer transferA = new Transfer(ACCOUNT_A, negativeOrZeroAmount, VESTING_SCHEDULE_DUMMY);
    Transfer transferB = new Transfer(ACCOUNT_B, 1, VESTING_SCHEDULE_DUMMY);
    List<Transfer> transfers = List.of(transferA, transferB);
    Hash message = initial.createTransferMessage(transfers, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMembers.subList(0, 3), message);

    Assertions.assertThatThrownBy(() -> invokeSignTransfer(initial, transfers, signatures))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No transfer amount can be negative or zero");
  }

  /**
   * A transfer with no signatures fails. This test is only for coverage. See {@link
   * FoundationMemberSignatureVerifierTest} for the exhaustive tests of verifying signatures.
   */
  @Test
  void transferNoSignatures() {
    Assertions.assertThatThrownBy(
            () ->
                invokeSignTransfer(
                    FoundationContractState.initial(
                        Long.MAX_VALUE, FixedList.create(createFoundationMembers())),
                    TRANSFERS_DUMMY,
                    List.of()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("At least three foundation members should have signed the transfers");
  }

  // Feature: Burn

  /**
   * A burn with three signatures from foundation members is executed. The amount of tokens to burn
   * is subtracted from the pool, and the nonce is bumped.
   *
   * <p>The amount of tokens to burn can be less than or equals to the amount of remaining tokens in
   * the pool.
   */
  @ParameterizedTest
  @ValueSource(ints = {99, 100})
  void burnSignedByAtLeastThreeFoundationMembers(int burnAmount) {
    int remainingTokens = 100;
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(remainingTokens, foundationMemberKeys);

    Hash message = initial.createBurnMessage(burnAmount, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMemberKeys.subList(0, 3), message);

    FoundationContractState afterBurn = invokeBurnTokens(initial, burnAmount, signatures);

    Assertions.assertThat(getRemainingTokens(afterBurn)).isEqualTo(remainingTokens - burnAmount);
    Assertions.assertThat(getNonce(afterBurn)).isEqualTo(getNonce(initial) + 1);
  }

  /**
   * A burn fails if the amount of tokens to burn exceeds the amount of remaining tokens in the
   * pool.
   */
  @Test
  void burnInsufficientTokensInPool() {
    int remainingTokens = 1;
    List<KeyPair> foundationMembers = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(remainingTokens, foundationMembers);

    int burnAmount = remainingTokens + 1;
    Hash message = initial.createBurnMessage(burnAmount, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMembers.subList(0, 3), message);

    Assertions.assertThatThrownBy(() -> invokeBurnTokens(initial, burnAmount, signatures))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Insufficient remaining tokens in pool to burn");
  }

  /**
   * A burn with no signatures fails. This test is only for coverage. See {@link
   * FoundationMemberSignatureVerifierTest} for the exhaustive tests of verifying signatures.
   */
  @Test
  void burnNoSignatures() {
    Assertions.assertThatThrownBy(
            () ->
                invokeBurnTokens(
                    FoundationContractState.initial(
                        Long.MAX_VALUE, FixedList.create(createFoundationMembers())),
                    1234,
                    List.of()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("At least three foundation members should have signed the burn");
  }

  // Feature: Approve

  /**
   * An approval with three signatures from foundation members is executed. The allowance is saved
   * to state, and the nonce is bumped.
   *
   * <p>The approve amount is relative and can exceed the size of the pool.
   */
  @ParameterizedTest
  @ValueSource(ints = {9, 10, 11})
  void approveSignedByAtLeastThreeFoundationMembers(int approveAmount) {
    long remainingTokens = 10;
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(remainingTokens, foundationMemberKeys);

    BlockchainAddress spender = ACCOUNT_A;
    List<Approval> approvals =
        List.of(new Approval(spender, approveAmount), new Approval(ACCOUNT_B, 1234));
    Hash message = initial.createApproveMessage(approvals, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMemberKeys.subList(0, 3), message);

    FoundationContractState afterApprove = invokeApprove(initial, approvals, signatures);

    Assertions.assertThat(getAllowances(afterApprove).size()).isEqualTo(approvals.size());
    Assertions.assertThat(getAllowances(afterApprove).getValue(spender)).isEqualTo(approveAmount);
    Assertions.assertThat(getNonce(afterApprove)).isEqualTo(getNonce(initial) + 1);
    Assertions.assertThat(afterApprove)
        .usingRecursiveComparison(StateSerializableEquality.ASSERTIONS_CONFIG_STATE_SERIALIZABLE)
        .ignoringFields("allowances", "nonce")
        .isEqualTo(initial);
  }

  private static AvlTree<BlockchainAddress, Long> getAllowances(FoundationContractState state) {
    return StateAccessor.create(state)
        .get("allowances")
        .typedAvlTree(BlockchainAddress.class, Long.class);
  }

  /**
   * Setting the amount of an allowance to zero or a negative amount removes the allowance from
   * state.
   */
  @Test
  void approveZeroOrNegativeAmountAllowanceIsRemoved() {
    long remainingTokens = 10;
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys(5);
    FoundationContractState initial = createInitial(remainingTokens, foundationMemberKeys);

    BlockchainAddress spender = ACCOUNT_A;
    long approveAmount = 5;
    List<Approval> approvals = List.of(new Approval(spender, approveAmount));
    Hash message = initial.createApproveMessage(approvals, context.getContractAddress());
    List<Signature> signatures = createSignatures(foundationMemberKeys, message);
    FoundationContractState afterApprove = invokeApprove(initial, approvals, signatures);

    Assertions.assertThat(getAllowances(afterApprove).size()).isEqualTo(1);

    // Set allowance amount to zero
    approvals = List.of(new Approval(spender, Math.negateExact(approveAmount)));
    message = afterApprove.createApproveMessage(approvals, context.getContractAddress());
    signatures = createSignatures(foundationMemberKeys, message);
    FoundationContractState afterApproveZero = invokeApprove(afterApprove, approvals, signatures);

    Assertions.assertThat(getAllowances(afterApproveZero).size()).isEqualTo(0);

    // Set allowance amount to a negative amount
    approvals = List.of(new Approval(spender, Math.negateExact(remainingTokens)));
    message = afterApprove.createApproveMessage(approvals, context.getContractAddress());
    signatures = createSignatures(foundationMemberKeys, message);
    FoundationContractState afterApproveNegative =
        invokeApprove(afterApprove, approvals, signatures);

    Assertions.assertThat(getAllowances(afterApproveNegative).size()).isEqualTo(0);
  }

  /**
   * An approval with no signatures fails. This test is only for coverage. See {@link
   * FoundationMemberSignatureVerifierTest} for the exhaustive tests of verifying signatures.
   */
  @Test
  void approveNoSignatures() {
    Assertions.assertThatThrownBy(
            () ->
                invokeApprove(
                    FoundationContractState.initial(
                        Long.MAX_VALUE, FixedList.create(createFoundationMembers())),
                    List.of(new Approval(ACCOUNT_A, 1234)),
                    List.of()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("At least three foundation members should have signed the approval");
  }

  // Feature: Mint

  /**
   * A mint from a sender with enough allowance and where the total minted amount does not exceed
   * the pool size is executed. The corresponding account plugin events are spawned, the allowance
   * of the sender is reduced by the total minted amount, the total minted amount is subtracted from
   * the pool. A mint does not bump the nonce.
   */
  @ParameterizedTest
  @ValueSource(ints = {1, 2})
  void mint(int mintAmount) {
    int remainingTokens = 3;
    FoundationContractState initial =
        createInitial(remainingTokens).approve(List.of(new Approval(ACCOUNT_A, remainingTokens)));

    List<Mint> mints = List.of(new Mint(ACCOUNT_B, mintAmount), new Mint(ACCOUNT_C, 1));

    setContextToAccountA();
    FoundationContractState afterMint = invokeMint(initial, mints);

    for (int i = 0; i < mints.size(); i++) {
      Mint mint = mints.get(i);
      LocalPluginStateUpdate localPluginStateUpdate =
          context.getUpdateLocalAccountPluginStates().get(i);
      Assertions.assertThat(localPluginStateUpdate.getContext()).isEqualTo(mint.recipient());
      byte[] actualRpc = localPluginStateUpdate.getRpc();
      byte[] expectedRpc = AccountPluginRpc.addMpcTokenBalance(mint.amount());
      Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);
    }

    long totalMintAmount = totalMintAmount(mints);
    Assertions.assertThat(getRemainingTokens(afterMint))
        .isEqualTo(remainingTokens - totalMintAmount);
    Assertions.assertThat(afterMint.getAllowance(ACCOUNT_A))
        .isEqualTo(remainingTokens - totalMintAmount);
    Assertions.assertThat(getNonce(afterMint)).isEqualTo(getNonce(initial));
  }

  /** Minting the exact amount of tokens of an allowance removes the allowance from state. */
  @Test
  void mintExactAllowance() {
    int allowanceAmount = 10;
    FoundationContractState initial =
        createInitial().approve(List.of(new Approval(ACCOUNT_A, allowanceAmount)));
    List<Mint> mints = List.of(new Mint(ACCOUNT_B, 5), new Mint(ACCOUNT_C, 5));
    setContextToAccountA();
    FoundationContractState afterMint = invokeMint(initial, mints);

    Assertions.assertThat(totalMintAmount(mints)).isEqualTo(allowanceAmount);
    Assertions.assertThat(getAllowances(afterMint).size()).isEqualTo(0);
  }

  /** The total amount of tokens to mint cannot exceed the allowance of the sender. */
  @Test
  void mintExceedsAllowance() {
    FoundationContractState initial = createInitial(createFoundationMemberKeys(5));

    // No allowance
    Assertions.assertThatThrownBy(() -> invokeMint(initial, List.of(new Mint(ACCOUNT_B, 1))))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Insufficient allowance for mints");

    // Total amount exceeds allowance
    int allowanceB = 1;
    FoundationContractState withAllowance =
        initial.approve(List.of(new Approval(ACCOUNT_A, allowanceB)));
    List<Mint> mints = List.of(new Mint(ACCOUNT_B, allowanceB), new Mint(ACCOUNT_C, 1));
    Assertions.assertThatThrownBy(() -> invokeMint(withAllowance, mints))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Insufficient allowance for mints");
  }

  /**
   * The total amount of tokens to mint cannot exceed the amount of remaining tokens in the pool.
   */
  @Test
  void mintExceedsTokenPool() {
    int remainingTokens = 3;
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys(5);
    List<Approval> approvals = List.of(new Approval(ACCOUNT_A, Long.MAX_VALUE));
    FoundationContractState initial =
        createInitial(remainingTokens, foundationMemberKeys).approve(approvals);

    Mint mintA = new Mint(ACCOUNT_B, remainingTokens);
    Mint mintB = new Mint(ACCOUNT_C, 1);
    List<Mint> mints = List.of(mintA, mintB);

    Assertions.assertThat(totalMintAmount(mints)).isGreaterThan(getRemainingTokens(initial));
    Assertions.assertThatThrownBy(() -> invokeMint(initial, mints))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Insufficient remaining tokens in pool for mints");
  }

  /** The mint amount cannot be negative. */
  @ParameterizedTest
  @ValueSource(ints = {-1, 0})
  void mintNegative(int negativeOrZeroAmount) {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys(5);
    List<Approval> approvals = List.of(new Approval(ACCOUNT_A, Long.MAX_VALUE));
    FoundationContractState initial = createInitial(foundationMemberKeys).approve(approvals);

    List<Mint> mints = List.of(new Mint(ACCOUNT_B, negativeOrZeroAmount), new Mint(ACCOUNT_C, 1));

    Assertions.assertThatThrownBy(() -> invokeMint(initial, mints))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No mint amount can be negative or zero");
  }

  private FoundationContractState invokeSignTransfer(
      FoundationContractState state, List<Transfer> transfers, List<Signature> signatures) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(FoundationContract.Invocations.SIGN_TRANSFER);
          s.writeInt(transfers.size());
          transfers.forEach(transfer -> transfer.write(s));
          s.writeInt(signatures.size());
          signatures.forEach(signature -> signature.write(s));
        });
  }

  private FoundationContractState invokeBurnTokens(
      FoundationContractState state, long amount, List<Signature> signatures) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(FoundationContract.Invocations.BURN_TOKENS);
          s.writeLong(amount);
          s.writeInt(signatures.size());
          signatures.forEach(signature -> signature.write(s));
        });
  }

  private FoundationContractState invokeApprove(
      FoundationContractState state, List<Approval> approvals, List<Signature> signatures) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(FoundationContract.Invocations.APPROVE);
          s.writeInt(approvals.size());
          approvals.forEach(a -> a.write(s));
          s.writeInt(signatures.size());
          signatures.forEach(signature -> signature.write(s));
        });
  }

  private FoundationContractState invokeMint(FoundationContractState state, List<Mint> mints) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(FoundationContract.Invocations.MINT);
          s.writeInt(mints.size());
          for (Mint mint : mints) {
            mint.recipient().write(s);
            s.writeLong(mint.amount());
          }
        });
  }

  static List<Signature> createSignatures(List<KeyPair> members, Hash message) {
    List<Signature> signatures = new ArrayList<>();
    for (KeyPair member : members) {
      signatures.add(member.sign(message));
    }
    return signatures;
  }

  private List<BlockchainPublicKey> createFoundationMembers() {
    return createFoundationMemberKeys(5).stream().map(KeyPair::getPublic).toList();
  }

  private List<BlockchainPublicKey> createFoundationMembers(int size) {
    return createFoundationMemberKeys(size).stream().map(KeyPair::getPublic).toList();
  }

  static List<KeyPair> createFoundationMemberKeys(int size) {
    List<KeyPair> foundationMemberKeys = new ArrayList<>();
    for (int i = 1; i <= size; i++) {
      foundationMemberKeys.add(new KeyPair(BigInteger.valueOf(i)));
    }
    return foundationMemberKeys;
  }

  private FoundationContractState createInitial() {
    return createInitial(Long.MAX_VALUE, createFoundationMemberKeys(5));
  }

  private FoundationContractState createInitial(List<KeyPair> foundationMemberKeys) {
    return createInitial(Long.MAX_VALUE, foundationMemberKeys);
  }

  private FoundationContractState createInitial(long remainingTokens) {
    return createInitial(remainingTokens, createFoundationMemberKeys(5));
  }

  private FoundationContractState createInitial(
      long pooledTokens, List<KeyPair> foundationMemberKeys) {
    return FoundationContractState.initial(
        pooledTokens, FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic)));
  }

  private void setContextToAccountA() {
    context =
        new SysContractContextTest(
            context.getBlockProductionTime(), context.getBlockTime(), ACCOUNT_A);
  }

  private long getNonce(FoundationContractState state) {
    return StateAccessor.create(state).get("nonce").longValue();
  }

  private long getRemainingTokens(FoundationContractState state) {
    return StateAccessor.create(state).get("remainingTokens").longValue();
  }

  private long totalTransferAmount(List<Transfer> transfers) {
    return transfers.stream().mapToLong(Transfer::amount).sum();
  }

  private long totalMintAmount(List<Mint> mints) {
    return mints.stream().mapToLong(Mint::amount).sum();
  }
}
