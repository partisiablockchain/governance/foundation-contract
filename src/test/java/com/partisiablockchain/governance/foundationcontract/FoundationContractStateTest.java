package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Test. */
final class FoundationContractStateTest {

  private static final BlockchainAddress TEST_ACCOUNT =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("TestAccount")));

  private static final BlockchainAddress TEST_CONTRACT =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(h -> h.writeString("TestContract")));

  /** Signature message of transfer stays consistent. */
  @Test
  void createTransferMessage() {
    FoundationContractState state =
        new FoundationContractState(99, 0, FixedList.create(), AvlTree.create());

    long transferAmount = 100_000;
    VestingSchedule vestingSchedule = new VestingSchedule(2, 3, 1);
    Transfer transfer = new Transfer(TEST_ACCOUNT, transferAmount, vestingSchedule);
    List<Transfer> transfers = List.of(transfer);

    Hash message = state.createTransferMessage(transfers, TEST_CONTRACT);
    Assertions.assertThat(message)
        .isEqualTo(
            Hash.fromString("b8729723bc5a26e7cb12001e72e51863c897c80e40ad60b2a35451dca319822b"));
  }

  /** Signature message of rotation stays consistent. */
  @DisplayName("The hash of the replace message is stable for the same message")
  @Test
  void createReplaceMessage() {
    FoundationContractState state =
        new FoundationContractState(22, 0, FixedList.create(), AvlTree.create());

    KeyPair keyPair = new KeyPair(BigInteger.TWO);
    NewCouncilKey updatedKey =
        new NewCouncilKey(
            keyPair.getPublic(), keyPair.sign(Hash.create(FunctionUtility.noOpConsumer())));
    List<NewCouncilKey> newCouncil = List.of(updatedKey);

    Hash message = state.createReplaceMessage(newCouncil, TEST_CONTRACT);
    Assertions.assertThat(message)
        .isEqualTo(
            Hash.fromString("96c891a81badda7f25b3ca3eb51b06866b6f671354c63b07fa1d927f9be21d2c"));
  }

  /** Signature message of burn stays consistent. */
  @Test
  void createBurnMessage() {
    FoundationContractState state =
        new FoundationContractState(99, 0, FixedList.create(), AvlTree.create());
    Hash signatureMessage = state.createBurnMessage(10_234_778, TEST_CONTRACT);
    Assertions.assertThat(signatureMessage)
        .isEqualTo(
            Hash.fromString("62fb7f9dbf6b1e389ac7e07fab63d6e09ef5d3fc4a51b0a58c21a6fc954bfe6d"));
  }

  /** Signature message of approve stays consistent. */
  @Test
  void createApproveMessage() {
    FoundationContractState state =
        new FoundationContractState(99, 0, FixedList.create(), AvlTree.create());
    Hash signatureMessage =
        state.createApproveMessage(List.of(new Approval(TEST_ACCOUNT, 1234)), TEST_CONTRACT);
    Assertions.assertThat(signatureMessage)
        .isEqualTo(
            Hash.fromString("89dfd05b00fbaa647116d7563799899f9fb704080b7f83f96aef986792d8c59a"));
  }
}
