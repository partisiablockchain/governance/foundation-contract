package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class FoundationMemberSignatureVerifierTest {

  private static final Hash MESSAGE_DUMMY = Hash.create(h -> h.writeInt(1234));
  private static final KeyPair MEMBER_A = new KeyPair(new BigInteger("A", 16));
  private static final KeyPair MEMBER_B = new KeyPair(new BigInteger("B", 16));
  private static final KeyPair MEMBER_C = new KeyPair(new BigInteger("C", 16));
  private static final KeyPair MEMBER_D = new KeyPair(new BigInteger("D", 16));
  private static final KeyPair MEMBER_E = new KeyPair(new BigInteger("E", 16));
  private static final KeyPair NON_MEMBER = new KeyPair(BigInteger.TWO);

  private final FoundationMemberSignatureVerifier signatureVerifier =
      new FoundationMemberSignatureVerifier(
          Stream.of(MEMBER_A, MEMBER_B, MEMBER_C, MEMBER_D, MEMBER_E)
              .map(KeyPair::getPublic)
              .toList());

  /** An empty list of signatures fails. */
  @Test
  void noSignatures() {
    Assertions.assertThat(signatureVerifier.verify(MESSAGE_DUMMY, List.of())).isFalse();
  }

  /** A list of signatures containing duplicate signatures fails. */
  @Test
  void duplicateSignatures() {
    Signature signatureA = MEMBER_A.sign(MESSAGE_DUMMY);
    Signature signatureB = MEMBER_B.sign(MESSAGE_DUMMY);
    List<Signature> signaturesWithDuplicates = List.of(signatureA, signatureB, signatureB);
    Assertions.assertThat(signatureVerifier.verify(MESSAGE_DUMMY, signaturesWithDuplicates))
        .isFalse();
  }

  /** A list of signatures with a signature from a non foundation member fails. */
  @Test
  void nonMemberSignature() {
    Signature signatureA = MEMBER_A.sign(MESSAGE_DUMMY);
    Signature signatureB = MEMBER_B.sign(MESSAGE_DUMMY);
    Signature signatureNonMember = NON_MEMBER.sign(MESSAGE_DUMMY);
    List<Signature> signaturesWithNonMember = List.of(signatureA, signatureB, signatureNonMember);

    FoundationMemberSignatureVerifier signatureVerifier =
        new FoundationMemberSignatureVerifier(List.of(MEMBER_A.getPublic(), MEMBER_B.getPublic()));

    Assertions.assertThat(signatureVerifier.verify(MESSAGE_DUMMY, signaturesWithNonMember))
        .isFalse();
  }

  /** Three or more signatures from foundation members succeeds. */
  @Test
  void threeOrMoreMemberSignatures() {
    // Three members
    Signature signatureA = MEMBER_A.sign(MESSAGE_DUMMY);
    Signature signatureB = MEMBER_B.sign(MESSAGE_DUMMY);
    Signature signatureC = MEMBER_C.sign(MESSAGE_DUMMY);
    List<Signature> signatures = List.of(signatureA, signatureB, signatureC);
    Assertions.assertThat(signatureVerifier.verify(MESSAGE_DUMMY, signatures)).isTrue();

    // Four members
    Signature signatureD = MEMBER_D.sign(MESSAGE_DUMMY);
    signatures = List.of(signatureA, signatureB, signatureC, signatureD);
    Assertions.assertThat(signatureVerifier.verify(MESSAGE_DUMMY, signatures)).isTrue();

    // Five members
    Signature signatureE = MEMBER_E.sign(MESSAGE_DUMMY);
    signatures = List.of(signatureA, signatureB, signatureC, signatureD, signatureE);
    Assertions.assertThat(signatureVerifier.verify(MESSAGE_DUMMY, signatures)).isTrue();
  }
}
