package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

final class NewCouncilKeyTest {

  private final KeyPair keyPair = new KeyPair(new BigInteger(32, new Random(1234)));
  private final NewCouncilKey newCouncilKey =
      new NewCouncilKey(
          keyPair.getPublic(),
          keyPair.sign(NewCouncilKey.createUniqueProofHash(keyPair.getPublic())));

  @DisplayName("Serialization of the NewCouncilKey is stable and contains record fields")
  @Test
  void write() {
    byte[] serialize = SafeDataOutputStream.serialize(newCouncilKey);
    Assertions.assertThat(serialize)
        .containsExactly(
            2, -56, 94, 63, 13, 69, 41, -60, -100, -63, 127, 90, -15, -109, -95, 12, 7, -125, -123,
            -115, 84, 99, 52, -6, -128, -83, 46, 32, -45, -103, -94, 80, 30, 1, 65, -99, 73, 2, -99,
            86, -16, -117, -14, -53, -4, 49, 29, 63, -81, 29, -82, 49, -86, -52, -9, -21, -77, 80,
            54, 113, 44, 40, -48, 7, 124, -108, 103, -82, -12, -1, 87, -6, -4, 104, -122, -44, -85,
            54, -14, -8, 126, -34, -23, -87, -37, -55, -91, 127, 35, 6, -38, 26, -42, -109, 101,
            -45, -75, 16);
  }

  @DisplayName("Correctly created signature is valid")
  @Test
  void checkValidPossessionProof() {
    Assertions.assertThat(newCouncilKey.checkValidPossessionProof()).isTrue();
  }

  @DisplayName("Incorrectly created signature is invalid")
  @Test
  void checkInvalidPossessionProof() {
    Assertions.assertThat(
            new NewCouncilKey(
                    keyPair.getPublic(), keyPair.sign(Hash.create(FunctionUtility.noOpConsumer())))
                .checkValidPossessionProof())
        .isFalse();
  }
}
