package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link FoundationContract} of the council key replacement, i.e. {@link
 * FoundationContract#replaceCouncilKeys(com.partisiablockchain.contract.sys.SysContractContext,
 * FoundationContractState, List, List) replaceCouncilKeys} .
 */
public final class FoundationContractReplaceCouncilKeysTest {

  private static final BlockchainAddress ACCOUNT_A =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("A")));
  private final SysContractContextTest context = new SysContractContextTest(1, 100, ACCOUNT_A);

  @DisplayName("Replaces keys for the council in the contract state with same keys (idempotent)")
  @Test
  public void replaceCouncilToSameKeys() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();
    List<NewCouncilKey> keyRotation = createRotation(foundationMemberKeys);
    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState(firstFoundationMembers);
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, keyRotation);
    FoundationContractState updated = callReplace(state, keyRotation, signatures);
    Assertions.assertThat(updated.foundationMembers)
        .hasSize(5)
        .containsAll(state.foundationMembers);
  }

  @DisplayName(
      "Replaces keys for the council without at least 3 signatures from the old council fails")
  @Test
  public void replaceCouncilRequiresAtLeastThreeSignature() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();
    List<NewCouncilKey> keyRotation = createRotation(foundationMemberKeys);
    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState(firstFoundationMembers);
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, keyRotation);
    signatures.remove(0);
    Assertions.assertThatThrownBy(() -> callReplace(state, keyRotation, signatures))
        .hasMessage("At least three foundation members should have signed the replacement");
  }

  @DisplayName("Replaces keys for the council in the contract state with council members changes")
  @Test
  public void replaceCouncilKeysReplace4MembersWith2NewAnd1Removed() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();

    List<NewCouncilKey> newFoundation = createRotation(foundationMemberKeys);
    newFoundation.remove(3);
    newFoundation.add(createRotation(newKeyPairFromSeed(123)));
    newFoundation.add(createRotation(newKeyPairFromSeed(321)));
    Assertions.assertThat(newFoundation).hasSize(6);

    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState(firstFoundationMembers);
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, newFoundation);
    FoundationContractState updated = callReplace(state, newFoundation, signatures);
    Assertions.assertThat(updated.foundationMembers)
        .hasSize(6)
        .containsAll(newFoundation.stream().map(NewCouncilKey::publicKey).toList())
        .contains(newKeyPairFromSeed(123).getPublic())
        .contains(newKeyPairFromSeed(321).getPublic());
  }

  private FoundationContractState callReplace(
      FoundationContractState state,
      List<NewCouncilKey> newFoundation,
      List<Signature> signatures) {
    SysContractSerialization<FoundationContractState> serialization =
        new SysContractSerialization<>(
            FoundationContractInvoker.class, FoundationContractState.class);
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(FoundationContract.Invocations.REPLACE_COUNCIL_KEYS);
          s.writeInt(newFoundation.size());
          newFoundation.forEach(transfer -> transfer.write(s));
          s.writeInt(signatures.size());
          signatures.forEach(signature -> signature.write(s));
        });
  }

  @DisplayName("Replaces keys for the council with a broken possession proof fails")
  @Test
  public void replaceCouncilBrokenPossessionProof() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();

    List<NewCouncilKey> newFoundation = createRotation(foundationMemberKeys);
    newFoundation.remove(3);
    KeyPair keyPair = newKeyPairFromSeed(123);
    Signature sign =
        keyPair.sign(Hash.create(newKeyPairFromSeed(666_666).getPublic().createAddress()));
    newFoundation.add(new NewCouncilKey(keyPair.getPublic(), sign));
    Assertions.assertThat(newFoundation).hasSize(5);

    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState(firstFoundationMembers);
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, newFoundation);
    Assertions.assertThatThrownBy(() -> callReplace(state, newFoundation, signatures))
        .hasMessage("All keys must be valid, i.e. able to do signing");
  }

  @DisplayName("Replaces keys for the council into a new, smaller council with four participants")
  @Test
  public void replaceKeyUpgradeReplaceWithSmallQuorum() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();
    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState2(firstFoundationMembers);

    List<NewCouncilKey> newFoundation = createReplacingCouncil();
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, newFoundation);

    FoundationContractState replaced = callReplace(state, newFoundation, signatures);
    Assertions.assertThat(replaced.foundationMembers).hasSize(4);
  }

  private static FoundationContractState createState2(
      FixedList<BlockchainPublicKey> firstFoundationMembers) {
    AvlTree<BlockchainAddress, Long> allowances = AvlTree.create(Map.of(ACCOUNT_A, 1111L));
    return createState(firstFoundationMembers, allowances);
  }

  private static FoundationContractState createState(
      FixedList<BlockchainPublicKey> firstFoundationMembers,
      AvlTree<BlockchainAddress, Long> allowances) {
    return new FoundationContractState(1234, 1000, firstFoundationMembers, allowances);
  }

  private static FoundationContractState createState(
      FixedList<BlockchainPublicKey> firstFoundationMembers) {
    return createState(firstFoundationMembers, AvlTree.create());
  }

  @DisplayName(
      "Replaces keys for the council into a new, smaller council with three participants fails")
  @Test
  public void replaceKeyUpgradeReplaceWithTooSmallQuorum() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();
    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState2(firstFoundationMembers);

    List<NewCouncilKey> newFoundation = createReplacingCouncil();
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, newFoundation);

    List<NewCouncilKey> smallFoundation = newFoundation.subList(0, 3);
    Assertions.assertThat(smallFoundation).hasSize(3);
    Assertions.assertThatThrownBy(() -> callReplace(state, smallFoundation, signatures))
        .hasMessage("Need at least 4 members in the foundation council");
  }

  private static List<NewCouncilKey> createReplacingCouncil() {
    return IntStream.range(0, 4)
        .mapToObj(FoundationContractReplaceCouncilKeysTest::newKeyPairFromSeed)
        .map(FoundationContractReplaceCouncilKeysTest::createRotation)
        .toList();
  }

  /** Rotating keys for the council with duplicates in the keys. */
  @DisplayName("Replaces keys for the council with non-unique keys fails")
  @Test
  public void replaceKeyUpgradeDuplicateKeys() {
    List<KeyPair> foundationMemberKeys = createFoundationMemberKeys();
    FixedList<BlockchainPublicKey> firstFoundationMembers =
        FixedList.create(foundationMemberKeys.stream().map(KeyPair::getPublic).toList());
    FoundationContractState state = createState2(firstFoundationMembers);

    KeyPair foundationMemberKey = newKeyPairFromSeed(1234);
    List<NewCouncilKey> newFoundation =
        IntStream.range(0, 5)
            .mapToObj(unused -> foundationMemberKey)
            .map(FoundationContractReplaceCouncilKeysTest::createRotation)
            .toList();
    List<Signature> signatures = createSignatures(state, foundationMemberKeys, newFoundation);

    Assertions.assertThatThrownBy(() -> callReplace(state, newFoundation, signatures))
        .hasMessage("All keys must be unique");
  }

  private List<Signature> createSignatures(
      FoundationContractState state,
      List<KeyPair> foundationMemberKeys,
      List<NewCouncilKey> newFoundation) {
    Hash message = state.createReplaceMessage(newFoundation, context.getContractAddress());
    return FoundationContractTest.createSignatures(foundationMemberKeys.subList(0, 3), message);
  }

  private static List<NewCouncilKey> createRotation(List<KeyPair> foundationMemberKeys) {
    List<NewCouncilKey> newFoundation = new ArrayList<>();
    for (KeyPair keyPair : foundationMemberKeys) {
      NewCouncilKey newCouncilKey = createRotation(keyPair);
      newFoundation.add(newCouncilKey);
    }
    return newFoundation;
  }

  private static NewCouncilKey createRotation(KeyPair keyPair) {
    Signature proof = keyPair.sign(NewCouncilKey.createUniqueProofHash(keyPair.getPublic()));
    return new NewCouncilKey(keyPair.getPublic(), proof);
  }

  private static KeyPair newKeyPairFromSeed(int i) {
    return new KeyPair(new BigInteger(32, new Random(i)));
  }

  private List<KeyPair> createFoundationMemberKeys() {
    return FoundationContractTest.createFoundationMemberKeys(5);
  }
}
