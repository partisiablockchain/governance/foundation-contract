package com.partisiablockchain.governance.foundationcontract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AccountPluginRpcTest {

  private static final BlockchainAddress ACCOUNT_A =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("A")));

  @Test
  void createVestingAccount() {
    long amount = 10;
    VestingSchedule vestingSchedule = new VestingSchedule(10, 2, 400);
    Transfer transfer = new Transfer(ACCOUNT_A, amount, vestingSchedule);
    byte[] rpc =
        AccountPluginRpc.createVestingAccount(transfer.amount(), transfer.vestingSchedule());
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.CREATE_VESTING_ACCOUNT);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(stream.readLong()).isEqualTo(10);
    assertThat(stream.readLong()).isEqualTo(2);
    assertThat(stream.readLong()).isEqualTo(400);
  }

  @Test
  void createAddMpcTokenBalance() {
    long amount = 1234;
    Mint mint = new Mint(ACCOUNT_A, amount);
    byte[] rpc = AccountPluginRpc.addMpcTokenBalance(mint.amount());
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ADD_MPC_TOKEN_BALANCE);
    assertThat(stream.readLong()).isEqualTo(amount);
  }
}
